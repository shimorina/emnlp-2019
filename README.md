Scripts, data, results for the paper

**[_Surface Realisation Using Full Delexicalisation_](https://www.aclweb.org/anthology/D19-1305/). A. Shimorina, C. Gardent. Proceedings of the 2019 Conference on Empirical Methods in Natural Language Processing and the 9th International Joint Conference on Natural Language Processing (EMNLP-IJCNLP).** [[poster](emnlp2019_poster.pdf)]

```
@inproceedings{shimorina-gardent-2019-surface,
    title = "Surface Realisation Using Full Delexicalisation",
    author = "Shimorina, Anastasia  and
      Gardent, Claire",
    booktitle = "Proceedings of the 2019 Conference on Empirical Methods in Natural Language Processing and the 9th International Joint Conference on Natural Language Processing (EMNLP-IJCNLP)",
    month = nov,
    year = "2019",
    address = "Hong Kong, China",
    publisher = "Association for Computational Linguistics",
    url = "https://www.aclweb.org/anthology/D19-1305",
    doi = "10.18653/v1/D19-1305",
    pages = "3084--3094"
}
```

## Requirements

* [SR'18 data](http://taln.upf.edu/pages/msr2018-ws/SRST.html#data)
* [SR'18 evaluation scripts](http://taln.upf.edu/pages/msr2018-ws/SRST.html#evaluation). The file `eval_Py3.py` was used for the evaluation.

# Code, data, results

Surface realisation code is stored in [this repository](https://gitlab.com/shimorina/msr-2019).

In this repository, only processed data and results are stored.

## Repository Structure

* `delex_dicts`

	Dictionaries for delexicalisation are stored here. There are two types of delexicalisation dicts:
	* `anonym-full` (maps id to lemma and MSF)
	* `anonym-path-full` (maps full-path-to-node to lemma and id)

* `morphology`
	* `data_for_reinflection` - input data for MR
	* `dual_forms` - ambiguous forms in UD data
	* `lemma_form_dicts` - learned morphological paradigms in json dictionaries
	* `results-sr18` - MR output

* `opennmtpy_models`

	* `fulldelex` - outputs with full delexicalisation
	* `nodelex_baseline` - baseline outputs with no delexicalisation 

* `processed_data`

	`shallow` - source data for WO
