1.
Lemma: abandonar	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: abandonasse	Freq: 1
Form: abandonassse	Freq: 1

2.
Lemma: abraçar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: abraça	Freq: 2
Form: abraçá	Freq: 1

3.
Lemma: aceitar	MSD: pos=VERB,Gender=Masc,Number=Sing,VerbForm=Part,Voice=Pass
Form: aceite	Freq: 2
Form: aceito	Freq: 1

4.
Lemma: acompanhar	MSD: pos=VERB,Gender=Masc,Number=Plur,VerbForm=Part
Form: acompanhados	Freq: 2
Form: acompanhas	Freq: 1

5.
Lemma: acto	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: acto	Freq: 6
Form: extracto	Freq: 1

6.
Lemma: afirmar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: afirma	Freq: 35
Form: re-afirma	Freq: 1

7.
Lemma: alto	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: superiores	Freq: 4
Form: altas	Freq: 3

8.
Lemma: alto	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: alta	Freq: 10
Form: altíssima	Freq: 1
Form: superior	Freq: 1

9.
Lemma: alto	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: superiores	Freq: 5
Form: altos	Freq: 4

10.
Lemma: alto	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: alto	Freq: 11
Form: superior	Freq: 9
Form: altíssimo	Freq: 1

11.
Lemma: aparecer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: apareceu	Freq: 5
Form: apariceu	Freq: 1

12.
Lemma: apoiar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: apoiam	Freq: 3
Form: apóiam	Freq: 1

13.
Lemma: apoiar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: apóia	Freq: 3
Form: apoia	Freq: 1

14.
Lemma: aposta	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: aposta	Freq: 3
Form: apostas	Freq: 1

15.
Lemma: apreender	MSD: pos=VERB,Gender=Masc,Number=Plur,VerbForm=Part
Form: apreendida	Freq: 1
Form: apreendidos	Freq: 1

16.
Lemma: apresentar	MSD: pos=VERB,VerbForm=Inf
Form: apresentar	Freq: 18
Form: apresentá	Freq: 1

17.
Lemma: aprovar	MSD: pos=VERB,VerbForm=Inf
Form: aprovar	Freq: 5
Form: aprová	Freq: 1

18.
Lemma: arquipélago	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: arquipélago	Freq: 5
Form: arquipálago	Freq: 1

19.
Lemma: atribuir	MSD: pos=VERB,Gender=Fem,Number=Plur,VerbForm=Part,Voice=Pass
Form: atribuidas	Freq: 1
Form: atribuídas	Freq: 1

20.
Lemma: atribuir	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Imp,VerbForm=Fin
Form: atribuiam	Freq: 1
Form: atribuíam	Freq: 1

21.
Lemma: bar	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: bares	Freq: 4
Form: bars	Freq: 1

22.
Lemma: bom	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: boas	Freq: 9
Form: melhores	Freq: 9

23.
Lemma: bom	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: boa	Freq: 42
Form: melhor	Freq: 23
Form: óptima	Freq: 1

24.
Lemma: bom	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: melhores	Freq: 15
Form: bons	Freq: 12

25.
Lemma: bom	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: bom	Freq: 46
Form: melhor	Freq: 36
Form: ótimo	Freq: 5

26.
Lemma: busca	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: busca	Freq: 10
Form: buscas	Freq: 1

27.
Lemma: característica	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: caracterísitica	Freq: 1
Form: característica	Freq: 1

28.
Lemma: cd	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: cd	Freq: 1
Form: cds	Freq: 1

29.
Lemma: científico	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: científico	Freq: 5
Form: cientifico	Freq: 1

30.
Lemma: começar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Past,VerbForm=Fin
Form: começaram	Freq: 4
Form: começarem	Freq: 1

31.
Lemma: concluir	MSD: pos=VERB,Gender=Fem,Number=Sing,VerbForm=Part
Form: concluida	Freq: 1
Form: concluída	Freq: 1

32.
Lemma: conduzir	MSD: pos=VERB,VerbForm=Inf
Form: conduzir	Freq: 6
Form: conduzi	Freq: 1

33.
Lemma: conseguir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: conseguiu	Freq: 26
Form: consegui	Freq: 1

34.
Lemma: conservar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: conserva	Freq: 2
Form: conservá	Freq: 1

35.
Lemma: consola	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: consola	Freq: 1
Form: consolas	Freq: 1

36.
Lemma: construir	MSD: pos=VERB,VerbForm=Ger
Form: construindo	Freq: 4
Form: construíndo	Freq: 1

37.
Lemma: continuar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: continua	Freq: 21
Form: continuá	Freq: 1

38.
Lemma: contribuir	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Past,VerbForm=Fin
Form: contribuíram	Freq: 3
Form: contribuiram	Freq: 1

39.
Lemma: coordenar	MSD: pos=VERB,VerbForm=Inf
Form: coordenar	Freq: 2
Form: coordená	Freq: 1

40.
Lemma: crítica	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: críticas	Freq: 17
Form: criticas	Freq: 1

41.
Lemma: cujo	MSD: pos=DET,Gender=Fem,Number=Plur,PronType=Rel
Form: cujas	Freq: 8
Form: cuja	Freq: 1

42.
Lemma: cumprir	MSD: pos=VERB,VerbForm=Inf
Form: cumprir	Freq: 8
Form: cumprí	Freq: 1

43.
Lemma: dar	MSD: pos=VERB,Gender=Fem,Number=Plur,VerbForm=Part
Form: dadas	Freq: 1
Form: dar	Freq: 1

44.
Lemma: dar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=1,Tense=Pres,VerbForm=Fin
Form: damos	Freq: 1
Form: demos	Freq: 1

45.
Lemma: dar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: dão	Freq: 19
Form: dêem	Freq: 1

46.
Lemma: definição	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: definição	Freq: 9
Form: definicão	Freq: 1

47.
Lemma: demo	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: demo	Freq: 1
Form: demos	Freq: 1

48.
Lemma: descoberta	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: descoberta	Freq: 5
Form: redescoberta	Freq: 1

49.
Lemma: desportivo	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: desportivo	Freq: 3
Form: gimno-desportivo	Freq: 1

50.
Lemma: digital	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: digitais	Freq: 1
Form: digital	Freq: 1

51.
Lemma: diminuir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: diminuiu	Freq: 1
Form: diminuíu	Freq: 1

52.
Lemma: diplomata	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: diplomatas	Freq: 4
Form: dilomatas	Freq: 1

53.
Lemma: distribuir	MSD: pos=VERB,Gender=Masc,Number=Plur,VerbForm=Part
Form: distribuídos	Freq: 4
Form: distribuidos	Freq: 1

54.
Lemma: distribuir	MSD: pos=VERB,VerbForm=Inf
Form: distribuir	Freq: 5
Form: distribuí	Freq: 1

55.
Lemma: divulgar	MSD: pos=VERB,VerbForm=Inf
Form: divulgar	Freq: 4
Form: divulgá	Freq: 1

56.
Lemma: dizer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: diz	Freq: 109
Form: dizê	Freq: 1

57.
Lemma: dois	MSD: pos=NUM,NumType=Card
Form: dois	Freq: 213
Form: duas	Freq: 107

58.
Lemma: dotar	MSD: pos=VERB,VerbForm=Inf
Form: dotar	Freq: 3
Form: dotá	Freq: 1

59.
Lemma: ela	MSD: pos=PRON,Case=Acc,Gender=Fem,Number=Sing,Person=3,PronType=Prs
Form: a	Freq: 41
Form: la	Freq: 22
Form: na	Freq: 2

60.
Lemma: elas	MSD: pos=PRON,Case=Acc,Gender=Fem,Number=Plur,Person=3,PronType=Prs
Form: as	Freq: 16
Form: las	Freq: 3
Form: nas	Freq: 2

61.
Lemma: ele	MSD: pos=PRON,Case=Acc,Gender=Masc,Number=Sing,Person=3,PronType=Prs
Form: o	Freq: 115
Form: lo	Freq: 30
Form: no	Freq: 2

62.
Lemma: ele	MSD: pos=PRON,Case=Dat,Gender=Masc,Number=Sing,Person=3,PronType=Prs
Form: lhe	Freq: 85
Form: ihe	Freq: 1

63.
Lemma: eles	MSD: pos=PRON,Case=Acc,Gender=Masc,Number=Plur,Person=3,PronType=Prs
Form: os	Freq: 26
Form: los	Freq: 8
Form: nos	Freq: 1

64.
Lemma: eletrônico	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: electrónica	Freq: 1
Form: eletrônica	Freq: 1

65.
Lemma: eletrônico	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: electrónicos	Freq: 3
Form: eletrônicos	Freq: 1

66.
Lemma: eletrônico	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: electrónico	Freq: 2
Form: eletrônico	Freq: 2

67.
Lemma: elevado	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: elevada	Freq: 2
Form: elevadíssima	Freq: 1

68.
Lemma: encarregar	MSD: pos=VERB,Gender=Masc,Number=Sing,VerbForm=Part
Form: encarregue	Freq: 2
Form: encarregado	Freq: 1

69.
Lemma: encontrar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=1,Tense=Pres,VerbForm=Fin
Form: encontramos	Freq: 3
Form: encontramo	Freq: 1

70.
Lemma: encontrar	MSD: pos=VERB,VerbForm=Inf
Form: encontrar	Freq: 27
Form: encontrá	Freq: 1

71.
Lemma: entregar	MSD: pos=VERB,Gender=Masc,Number=Sing,VerbForm=Part,Voice=Pass
Form: entregue	Freq: 5
Form: entrega	Freq: 1

72.
Lemma: escalar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: escala	Freq: 1
Form: escalou	Freq: 1

73.
Lemma: espacial	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: espacais	Freq: 1
Form: espaciais	Freq: 1

74.
Lemma: estar	MSD: pos=AUX,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: estão	Freq: 117
Form: está	Freq: 1

75.
Lemma: estar	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: está	Freq: 267
Form: tá	Freq: 3

76.
Lemma: este	MSD: pos=DET,Gender=Masc,Number=Plur,PronType=Dem
Form: estes	Freq: 44
Form: este	Freq: 1

77.
Lemma: estrear	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: estréia	Freq: 3
Form: estreia	Freq: 2

78.
Lemma: estudo	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: estudo	Freq: 21
Form: mestudo	Freq: 1

79.
Lemma: europeu	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: europeia	Freq: 14
Form: européia	Freq: 3

80.
Lemma: exacto	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: exacta	Freq: 2
Form: exactas	Freq: 1

81.
Lemma: exemplar	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: exemplares	Freq: 2
Form: ecxemplares	Freq: 1

82.
Lemma: fazer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: fez	Freq: 64
Form: fê	Freq: 1

83.
Lemma: fazer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: faz	Freq: 49
Form: fazê	Freq: 3
Form: fá	Freq: 1

84.
Lemma: fazer	MSD: pos=VERB,VerbForm=Inf
Form: fazer	Freq: 123
Form: fazê	Freq: 3

85.
Lemma: fazer	MSD: pos=VERB,VerbForm=Part
Form: feito	Freq: 15
Form: feitas	Freq: 1

86.
Lemma: ganhar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: ganha	Freq: 10
Form: ganhá	Freq: 1

87.
Lemma: gerir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: gere	Freq: 1
Form: gira	Freq: 1

88.
Lemma: grande	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: grandes	Freq: 29
Form: maiores	Freq: 11

89.
Lemma: grande	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: maior	Freq: 75
Form: grande	Freq: 58
Form: máxima	Freq: 3

90.
Lemma: grande	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: grandes	Freq: 31
Form: maiores	Freq: 12
Form: máximos	Freq: 1

91.
Lemma: grande	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: maior	Freq: 45
Form: grande	Freq: 44
Form: máximo	Freq: 6

92.
Lemma: guy	MSD: pos=PROPN,Gender=Masc,Number=Plur
Form: guy	Freq: 1
Form: guys	Freq: 1

93.
Lemma: haver	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: há	Freq: 4
Form: há-	Freq: 1

94.
Lemma: helicóptero	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: helicópteros	Freq: 2
Form: porta-helicópteros	Freq: 1

95.
Lemma: hora	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: hora	Freq: 34
Form: -hora	Freq: 1

96.
Lemma: impor	MSD: pos=VERB,VerbForm=Inf
Form: impor	Freq: 8
Form: impôr	Freq: 1

97.
Lemma: importante	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: importante	Freq: 21
Form: importanle	Freq: 1

98.
Lemma: inaugurar	MSD: pos=VERB,Gender=Fem,Number=Sing,VerbForm=Part,Voice=Pass
Form: inaugurada	Freq: 4
Form: inugurada	Freq: 1

99.
Lemma: incorporar	MSD: pos=VERB,VerbForm=Inf
Form: incorporar	Freq: 2
Form: incorporá	Freq: 1

100.
Lemma: internacional	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: internacionais	Freq: 12
Form: internacionas	Freq: 1

101.
Lemma: interromper	MSD: pos=VERB,VerbForm=Inf
Form: interromper	Freq: 3
Form: interrompê	Freq: 1

102.
Lemma: investigador	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: investigador	Freq: 3
Form: investigadores	Freq: 1

103.
Lemma: ir	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Fut,VerbForm=Fin
Form: for	Freq: 5
Form: foi	Freq: 1

104.
Lemma: item	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: items	Freq: 1
Form: itens	Freq: 1

105.
Lemma: jogar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: joga	Freq: 9
Form: jogá	Freq: 1

106.
Lemma: jogo	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: jogo	Freq: 73
Form: jogos	Freq: 1

107.
Lemma: lançar	MSD: pos=VERB,VerbForm=Inf
Form: lançar	Freq: 14
Form: lançá	Freq: 1

108.
Lemma: levar	MSD: pos=VERB,VerbForm=Inf
Form: levar	Freq: 15
Form: levá	Freq: 1

109.
Lemma: localizar	MSD: pos=VERB,VerbForm=Inf
Form: localizar	Freq: 1
Form: localizá	Freq: 1

110.
Lemma: líquido	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: líquido	Freq: 7
Form: liquido	Freq: 1

111.
Lemma: mais	MSD: pos=PRON,Gender=Masc,Number=Sing,PronType=Rel
Form: quanto	Freq: 1
Form: tudo	Freq: 1

112.
Lemma: mal	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: péssimo	Freq: 2
Form: mal	Freq: 1

113.
Lemma: mandar	MSD: pos=VERB,VerbForm=Inf
Form: mandar	Freq: 5
Form: mandá	Freq: 1

114.
Lemma: manter	MSD: pos=VERB,VerbForm=Inf
Form: manter	Freq: 35
Form: mantê	Freq: 2

115.
Lemma: matar	MSD: pos=VERB,VerbForm=Inf
Form: matar	Freq: 4
Form: matá	Freq: 1

116.
Lemma: mau	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: pior	Freq: 3
Form: má	Freq: 1

117.
Lemma: mau	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: maus	Freq: 6
Form: piores	Freq: 2

118.
Lemma: mau	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: mau	Freq: 16
Form: pior	Freq: 5

119.
Lemma: medir	MSD: pos=VERB,VerbForm=Inf
Form: medir	Freq: 2
Form: medi	Freq: 1

120.
Lemma: meio	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: meias	Freq: 1
Form: meios	Freq: 1

121.
Lemma: mesmo	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: mesmo	Freq: 79
Form: mesmos	Freq: 3

122.
Lemma: metade	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: metade	Freq: 31
Form: metada	Freq: 1

123.
Lemma: meu	MSD: pos=DET,Gender=Fem,Number=Plur,PronType=Prs
Form: minhas	Freq: 8
Form: suas	Freq: 3
Form: sua	Freq: 1

124.
Lemma: meu	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Prs
Form: minha	Freq: 23
Form: sua	Freq: 3
Form: seu	Freq: 1
Form: seus	Freq: 1

125.
Lemma: meu	MSD: pos=DET,Gender=Masc,Number=Plur,PronType=Prs
Form: seus	Freq: 3
Form: meus	Freq: 2
Form: nossos	Freq: 1
Form: seu	Freq: 1

126.
Lemma: meu	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Prs
Form: meu	Freq: 34
Form: seu	Freq: 2

127.
Lemma: mil	MSD: pos=NUM,NumType=Card
Form: mil	Freq: 170
Form: miul	Freq: 1

128.
Lemma: milhão	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: milhões	Freq: 165
Form: mi	Freq: 1

129.
Lemma: morrer	MSD: pos=VERB,Gender=Masc,Number=Sing,VerbForm=Part
Form: morto	Freq: 5
Form: morrido	Freq: 1

130.
Lemma: mudar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: mudou	Freq: 5
Form: muda	Freq: 1

131.
Lemma: muito	MSD: pos=DET,Gender=Fem,Number=Plur,PronType=Ind
Form: muitas	Freq: 21
Form: mais	Freq: 13

132.
Lemma: muito	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Ind
Form: mais	Freq: 9
Form: muita	Freq: 8

133.
Lemma: muito	MSD: pos=DET,Gender=Masc,Number=Plur,PronType=Ind
Form: muitos	Freq: 42
Form: mais	Freq: 16

134.
Lemma: muito	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Ind
Form: muito	Freq: 24
Form: mais	Freq: 23

135.
Lemma: muito	MSD: pos=PRON,Gender=Fem,Number=Plur,PronType=Ind
Form: muitas	Freq: 5
Form: mais	Freq: 1

136.
Lemma: muito	MSD: pos=PRON,Gender=Fem,Number=Sing,PronType=Ind
Form: mais	Freq: 1
Form: muita	Freq: 1

137.
Lemma: muito	MSD: pos=PRON,Gender=Masc,Number=Plur,PronType=Ind
Form: muitos	Freq: 14
Form: mais	Freq: 1

138.
Lemma: muito	MSD: pos=PRON,Gender=Masc,Number=Sing,PronType=Ind
Form: muito	Freq: 10
Form: mais	Freq: 5

139.
Lemma: nacional	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: nacionais	Freq: 7
Form: nacinais	Freq: 1

140.
Lemma: negociar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: negociam	Freq: 2
Form: negoceiam	Freq: 1

141.
Lemma: negociar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: negocia	Freq: 3
Form: negoceia	Freq: 1

142.
Lemma: o	MSD: pos=DET,Definite=Def,Gender=Fem,Number=Sing,PronType=Art
Form: a	Freq: 8414
Form: as	Freq: 1

143.
Lemma: o	MSD: pos=DET,Definite=Def,Gender=Fem,PronType=Art
Form: a	Freq: 1
Form: as	Freq: 1

144.
Lemma: o	MSD: pos=DET,Definite=Def,Gender=Masc,Number=Plur,PronType=Art
Form: os	Freq: 3313
Form: o	Freq: 1

145.
Lemma: o	MSD: pos=DET,Definite=Def,Gender=Masc,Number=Sing,PronType=Art
Form: o	Freq: 10233
Form: a	Freq: 1
Form: os	Freq: 1

146.
Lemma: o	MSD: pos=DET,Definite=Def,PronType=Art
Form: a	Freq: 981
Form: as	Freq: 224

147.
Lemma: o	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Art
Form: a	Freq: 24
Form: o	Freq: 1

148.
Lemma: o	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Art
Form: o	Freq: 55
Form: 0	Freq: 1

149.
Lemma: o	MSD: pos=PRON,Gender=Masc,Number=Sing,PronType=Dem
Form: o	Freq: 380
Form: os	Freq: 1

150.
Lemma: oito	MSD: pos=NUM,NumType=Card
Form: oito	Freq: 23
Form: oitos	Freq: 1

151.
Lemma: pele	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: pele	Freq: 1
Form: peles	Freq: 1

152.
Lemma: pequeno	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: pequenas	Freq: 13
Form: menores	Freq: 2
Form: mínimas	Freq: 1

153.
Lemma: pequeno	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: pequena	Freq: 18
Form: menor	Freq: 11
Form: mínima	Freq: 5

154.
Lemma: pequeno	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: pequenos	Freq: 18
Form: menores	Freq: 3

155.
Lemma: pequeno	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: pequeno	Freq: 13
Form: menor	Freq: 8
Form: mínimo	Freq: 8

156.
Lemma: perda	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: perda	Freq: 13
Form: perde	Freq: 1

157.
Lemma: perspectiva	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: perspectiva	Freq: 14
Form: prespectiva	Freq: 1

158.
Lemma: pertencer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: pertencia	Freq: 2
Form: pertência	Freq: 1

159.
Lemma: pmdb	MSD: pos=PROPN,Gender=Masc,Number=Sing
Form: pmdb	Freq: 22
Form: psdb	Freq: 1

160.
Lemma: pobre	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: pobre	Freq: 8
Form: paupérrimo	Freq: 1

161.
Lemma: posto	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: posto	Freq: 8
Form: postos	Freq: 3

162.
Lemma: pouco	MSD: pos=DET,Gender=Fem,Number=Plur,PronType=Ind
Form: poucas	Freq: 10
Form: menos	Freq: 2

163.
Lemma: pouco	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Ind
Form: menos	Freq: 6
Form: pouca	Freq: 6

164.
Lemma: pouco	MSD: pos=DET,Gender=Masc,Number=Plur,PronType=Ind
Form: poucos	Freq: 18
Form: menos	Freq: 4

165.
Lemma: pouco	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Ind
Form: pouco	Freq: 29
Form: menos	Freq: 7

166.
Lemma: pouco	MSD: pos=PRON,Gender=Masc,Number=Plur,PronType=Dem
Form: poucos	Freq: 3
Form: menos	Freq: 2

167.
Lemma: prejuízo	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: prejuízos	Freq: 6
Form: prejuizos	Freq: 2

168.
Lemma: primeiro	MSD: pos=ADJ,Gender=Fem,Number=Sing,NumType=Ord
Form: primeira	Freq: 101
Form: primeiro	Freq: 2

169.
Lemma: procurar	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: procuram	Freq: 3
Form: procuras	Freq: 1

170.
Lemma: produzir	MSD: pos=VERB,VerbForm=Inf
Form: produzir	Freq: 9
Form: produzí	Freq: 1

171.
Lemma: propina	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: propina	Freq: 1
Form: propinas	Freq: 1

172.
Lemma: proprietário	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: proprietários	Freq: 3
Form: propietários	Freq: 1

173.
Lemma: público	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: público	Freq: 44
Form: púbico	Freq: 1

174.
Lemma: que	MSD: pos=PRON,Gender=Fem,Number=Sing,PronType=Rel
Form: que	Freq: 527
Form: qu	Freq: 1

175.
Lemma: queixar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: queixa	Freq: 4
Form: queixou	Freq: 1

176.
Lemma: realização	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: realização	Freq: 19
Form: realiazação	Freq: 1

177.
Lemma: recolher	MSD: pos=VERB,Gender=Fem,Number=Plur,VerbForm=Part,Voice=Pass
Form: recohidas	Freq: 1
Form: recolhidas	Freq: 1

178.
Lemma: representante	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: representante	Freq: 7
Form: reprsentante	Freq: 1

179.
Lemma: requerer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: requer	Freq: 1
Form: requere	Freq: 1

180.
Lemma: resolver	MSD: pos=VERB,VerbForm=Inf
Form: resolver	Freq: 17
Form: resolvê	Freq: 1

181.
Lemma: retrato	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: retratos	Freq: 2
Form: auto-retratos	Freq: 1

182.
Lemma: roubar	MSD: pos=VERB,VerbForm=Inf
Form: roubar	Freq: 1
Form: roubá	Freq: 1

183.
Lemma: sair	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: saem	Freq: 8
Form: saiem	Freq: 1

184.
Lemma: sandes	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: sandes	Freq: 1
Form: sandesitas	Freq: 1

185.
Lemma: santo	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: santa	Freq: 2
Form: santíssima	Freq: 1

186.
Lemma: se	MSD: pos=PRON,Case=Acc,Gender=Fem,Number=Sing,Person=3,PronType=Prs
Form: se	Freq: 252
Form: se-	Freq: 1

187.
Lemma: se	MSD: pos=PRON,Case=Acc,Gender=Masc,Number=Plur,Person=3,PronType=Prs
Form: se	Freq: 208
Form: se-	Freq: 1

188.
Lemma: se	MSD: pos=PRON,Case=Acc,Gender=Masc,Number=Sing,Person=3,PronType=Prs
Form: se	Freq: 414
Form: s	Freq: 1

189.
Lemma: se	MSD: pos=PRON,Gender=Unsp,PronType=Prs
Form: se	Freq: 2
Form: si	Freq: 2

190.
Lemma: segundo	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: segundos	Freq: 6
Form: '	Freq: 1

191.
Lemma: ser	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=1,Tense=Pres,VerbForm=Fin
Form: sou	Freq: 9
Form: sô	Freq: 2

192.
Lemma: ser	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: foi	Freq: 440
Form: foram	Freq: 1

193.
Lemma: ser	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: é	Freq: 1054
Form: eis	Freq: 2
Form: sou	Freq: 1
Form: çe	Freq: 1

194.
Lemma: ser	MSD: pos=AUX,Mood=Sub,Number=Sing,Person=3,Tense=Fut,VerbForm=Fin
Form: for	Freq: 24
Form: fôr	Freq: 1

195.
Lemma: ser	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: é	Freq: 88
Form: eis	Freq: 1

196.
Lemma: seu	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Prs
Form: sua	Freq: 391
Form: seu	Freq: 1

197.
Lemma: sofrer	MSD: pos=VERB,VerbForm=Inf
Form: sofrer	Freq: 5
Form: sofrê	Freq: 1

198.
Lemma: submeter	MSD: pos=VERB,VerbForm=Inf
Form: submeter	Freq: 4
Form: ser	Freq: 1

199.
Lemma: substituir	MSD: pos=VERB,VerbForm=Inf
Form: substituir	Freq: 4
Form: substituí	Freq: 1

200.
Lemma: sujeito	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: sujeita	Freq: 1
Form: sujeito	Freq: 1

201.
Lemma: supor	MSD: pos=VERB,VerbForm=Inf
Form: supor	Freq: 2
Form: supôr	Freq: 1

202.
Lemma: surpreendente	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: surpreendente	Freq: 3
Form: supreendente	Freq: 1

203.
Lemma: sábado	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: sábado	Freq: 25
Form: sáb	Freq: 1

204.
Lemma: sócio	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: sócio	Freq: 3
Form: sócios	Freq: 1

205.
Lemma: taxa	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: taxas	Freq: 21
Form: taxa	Freq: 1

206.
Lemma: taxa	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: taxa	Freq: 30
Form: taxas	Freq: 1

207.
Lemma: ter	MSD: pos=AUX,VerbForm=Inf
Form: ter	Freq: 120
Form: tê	Freq: 2

208.
Lemma: ter	MSD: pos=VERB,VerbForm=Inf
Form: ter	Freq: 85
Form: tê	Freq: 1

209.
Lemma: termo	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: termo	Freq: 9
Form: termos	Freq: 1

210.
Lemma: todo	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Tot
Form: toda	Freq: 59
Form: todo	Freq: 4

211.
Lemma: tonelada	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: toneladas	Freq: 14
Form: toenaladas	Freq: 1

212.
Lemma: tornar	MSD: pos=VERB,VerbForm=Inf
Form: tornar	Freq: 13
Form: torná	Freq: 2

213.
Lemma: trabalho	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: trabalhos	Freq: 12
Form: trabalho	Freq: 2

214.
Lemma: tradicional	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: tradicional	Freq: 5
Form: tradiciona	Freq: 1

215.
Lemma: treinar	MSD: pos=VERB,VerbForm=Inf
Form: treinar	Freq: 2
Form: treiná	Freq: 1

216.
Lemma: um	MSD: pos=DET,Definite=Ind,Gender=Fem,Number=Sing,PronType=Art
Form: uma	Freq: 1339
Form: um	Freq: 1

217.
Lemma: um	MSD: pos=NUM,NumType=Card
Form: um	Freq: 241
Form: uma	Freq: 108

218.
Lemma: vacinar	MSD: pos=VERB,VerbForm=Inf
Form: vacinar	Freq: 1
Form: vaciná	Freq: 1

219.
Lemma: variado	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: variados	Freq: 1
Form: variadíssimos	Freq: 1

220.
Lemma: venda	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: venda	Freq: 34
Form: vendas	Freq: 1

221.
Lemma: vender	MSD: pos=VERB,VerbForm=Inf
Form: vender	Freq: 3
Form: vende	Freq: 1

222.
Lemma: ver	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: vêem	Freq: 3
Form: viram	Freq: 1

223.
Lemma: vez	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: vez	Freq: 63
Form: vezes	Freq: 1

224.
Lemma: videojogo	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: videojogos	Freq: 5
Form: vídeojogos	Freq: 2

225.
Lemma: visitar	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: visita	Freq: 4
Form: visitá	Freq: 1

226.
Lemma: visitar	MSD: pos=VERB,VerbForm=Inf
Form: visitar	Freq: 6
Form: visitá	Freq: 1

227.
Lemma: você	MSD: pos=PRON,Case=Nom,Gender=Unsp,Number=Sing,Person=3,PronType=Prs
Form: você	Freq: 24
Form: voce	Freq: 1

228.
Lemma: órgão	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: órgãos	Freq: 4
Form: orgãos	Freq: 1
